#include <stdio.h>
 
int main(){
   
    int numero1;
    int numero2;
    int multiplicacion=0;
    int contador = 0;
 
    printf("Dame un numero\n");
    scanf("%d", &numero1);
    getchar();
   
    printf("Dame otro numero\n");
    scanf("%d", &numero2);
    getchar();
 
    while(contador<numero2){
       multiplicacion = multiplicacion + numero1;
       contador++; // contador = contador + 1;      
    }
       
    printf("Resultado = %dx%d = %d\n",numero1,numero2,multiplicacion);
   
    getchar();
   
    return 0;
}